Wiki Setup
==========

Deploy live and test instances of the wiki.

The following programs should be available in your system to run the deployment script:

	chmod chown cp gpg make mkdir mv patch tar touch wget

You do also need `python` to run the standalone wiki server and the Apache web server with mod_wsgi to run the live version.

Deploying a test instance
-------------------------

Some tests can conveniently be run in MoinMoin's standalone server. To set up a test instance as a user, first set up a file named `config.mk`:

    INSTALLDIR = installtest
    SERVICEDIR = wikitest

Naturally the directory names can be choosen as you like. The directories should be at a user writable location. Putting them into this directory is okay, as long as you don't commit them by accident ;-)

Run `make test`. The script will download and verify the tarball for Moinwiki. It will also download plugins and apply patches. Fire up the standalone server by typing `./wikiserver.py`. Point your browser to http://localhost:8080/.

Deploying a live instance
-------------------------

Run `make service` as root. Unless otherwise specified the wiki will be installed to /opt and a data directory will be set up in /srv. Use `config.mk` to change those defaults. The Apache configuration file for "scheele" (FSFE's wiki server) will not be copied automatically. It is included and can be used as appropriate.
