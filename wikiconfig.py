# -*- coding: iso-8859-1 -*-
# IMPORTANT! This encoding (charset) setting MUST be correct! If you live in a
# western country and you don't know that you use utf-8, you probably want to
# use iso-8859-1 (or some other iso charset). If you use utf-8 (a Unicode
# encoding) you MUST use: coding: utf-8
# That setting must match the encoding your editor uses when you modify the
# settings below. If it does not, special non-ASCII chars will be wrong.

"""
    MoinMoin - Configuration for a single wiki

    If you run a single wiki only, you can omit the farmconfig.py config
    file and just use wikiconfig.py - it will be used for every request
    we get in that case.

    Note that there are more config options than you'll find in
    the version of this file that is installed by default; see
    the module MoinMoin.config.multiconfig for a full list of names and their
    default values.

    Also, the URL http://moinmo.in/HelpOnConfiguration has
    a list of config options.

    ** Please do not use this file for a wiki farm. Use the sample file
    from the wikifarm directory instead! **
"""

import os

from MoinMoin.config import multiconfig, url_prefix_static


class Config(multiconfig.DefaultConfig):
    # for help see https://moinmo.in/HelpOnConfiguration

    # Critical setup  ---------------------------------------------------
    wikiconfig_dir = os.path.abspath(os.path.dirname(__file__))

    instance_dir = wikiconfig_dir
    data_dir = os.path.join(instance_dir, 'data', '') # path with trailing /
    data_underlay_dir = os.path.join(instance_dir, 'underlay', '') # path with trailing /

    # Wiki identity ----------------------------------------------------
    sitename = u'FSFE Wiki'
    url_prefix = url_prefix_static  # backwards compatibility for EventCalendar Macro
    logo_string = u'<img src="%s/fsfe/img/logo_transparent.png" alt="Fellowship Logo">' % url_prefix_static
    page_front_page = u"FSFE Wiki"

    # Security ----------------------------------------------------------
    superuser = []
    acl_rights_before = u"albert,eal,jonas,mk:read,write,revert,delete,admin"
    acl_rights_default = u'Group/WikiCareTakers:read,write,revert,delete,admin'
    acl_rights_after = u'All:read'
    acl_hierarchic = True

    from MoinMoin.security.antispam import SecurityPolicy

    textchas = { 'en': {
        u"The abilities to run, study, share and improve software are known as the four...": ur"freedoms?",
	# Dropped, because Fellowship brand discontinued
        # u"What is the name of the group of FSFE members for whom this wiki is primarily intended?": ur"fellows(hip)?",
        u"This organisation promotes Free Software on which continent?": ur"europe",
        u"What does the 'E' in FSFE stand for?": ur"europe",
        u"What does the 'S' in FSFE stand for?": ur"software",
        #u"Give the initials of a sister organisation of this one!": ur"(FSF|FSFI|FSFLA)",
    }, }
    textchas_disabled_group = u"Group/Registered"

    surge_action_limits = {
        'show': (15,20),
        'raw': (20,40),
        'AttachFile': (100,5),
        'diff': (40,60),
        'fullsearch': (30,60),
        'edit': (15,30),
        'rss_rc': (20,60),
        'default': (30,60),
    }
    surge_lockout_time = 180

    # Mail --------------------------------------------------------------
    mail_smarthost = "mail.fsfeurope.org"
    mail_from = u"FSFE Wiki <admin@fsfe.org>"

    # User interface ----------------------------------------------------
    theme_default = 'fsfe'

    user_homewiki = 'Fellows'

    # Xapian search engine ----------------------------------------------
    xapian_search = True
    xapian_stemming = False

    # Language options --------------------------------------------------
    language_default = 'en'

    page_category_regex = ur'(?P<all>Category/(?P<key>\S+))'
    page_dict_regex = ur'(?P<all>Dictionary/(?P<key>\S+))'
    page_group_regex = ur'(?P<all>Group/(?P<key>\S+))'
    page_template_regex = ur'(?P<all>Template/(?P<key>\S+))'

    # Content options ---------------------------------------------------
    show_hosts = 0

    user_form_disable = ['name', 'aliasname', 'email', ]
    user_form_remove = ['password', 'password2', 'logout', 'create', 'account_sendmail', ]
    actions_excluded = ['recoverpass', 'MyPages', ]

    page_license_enabled = True
    page_license_page = u'LicensingPolicy'

    event_aggregator_new_event_template = 'FellowshipEventsTemplate'

    editor_default = 'text'
    editor_force = True

    smileys = (r"/!\ <!> (!) (./) {OK} {X} {i} {1} {2} {3} {*} {o}").split()

    # LDAP Configuration ------------------------------------------------
    from MoinMoin.auth.ldap_login import LDAPAuth
    ldap_authenticator1 = LDAPAuth(
        server_uri='ldap://ldap.fsfeurope.org',
        bind_dn='uid=%(username)s,ou=fellowship,dc=fsfe,dc=org',
        bind_pw='%(password)s',
        base_dn='ou=fellowship,dc=fsfe,dc=org',
        scope=2,
        referrals=0,
        search_filter='(&(uid=%(username)s)(!(uid=*@*)))',
        givenname_attribute='givenName',
        surname_attribute='sn',
        aliasname_attribute='displayName',
        email_attribute='mail',
        email_callback=None,
        coding='utf-8',
        timeout=10,
        start_tls=0,
        tls_cacertdir='',
        tls_cacertfile='',
        tls_certfile='',
        tls_keyfile='',
        tls_require_cert=0,
        bind_once=False,
        autocreate=True,
    )

    #from MoinMoin.auth import MoinAuth
    auth = [ldap_authenticator1]
    cookie_lifetime = (0, 12)
