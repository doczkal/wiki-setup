# -*- coding: iso-8859-1 -*-
"""
    FSFE Theme

    containing basic code structures from MoinMoin ThemeBase by Thomas Waldmann

    @copyright: 2016 Paul Hänsch <paul@fsfe.org> while working for Free Software Foundation Europe

    @license: GNU GPL, see COPYING for details.
"""

import StringIO
from MoinMoin.theme import ThemeBase
from MoinMoin import wikiutil
from MoinMoin.Page import Page

class Theme(ThemeBase):

    name = "fsfe"

    def navibar(self, d, **keywords):
        """ Display page called NavigationBar as an additional element on every page

        @param d: parameter dictionary
        @rtype: string
        @return: navibar html
        """

        # Check which page to display, return nothing if doesn't exist.
        navibar = self.request.getPragma('navibar', u'WikiAdmin/NavigationBar')
        page = Page(self.request, navibar)
        if not page.exists():
            return ThemeBase.navibar(self, d, **keywords)
        # Capture the page's generated HTML in a buffer.
        buffer = StringIO.StringIO()
        self.request.redirect(buffer)
        try:
            page.send_page(content_only=1, content_id="navibar")
        finally:
            self.request.redirect()
        return u'<div id="navibar">%s</div>' % buffer.getvalue()

    def searchform(self, d):
        """
        assemble HTML code for the search forms

        @param d: parameter dictionary
        @rtype: unicode
        @return: search form html
        """
        _ = self.request.getText
        form = self.request.values
        updates = {
            'search_label': _('Search:'),
            'search_value': wikiutil.escape(form.get('value', ''), 1),
            'search_full_label': _('Text'),
            'search_title_label': _('Titles'),
            'url': self.request.href(d['page'].page_name)
            }
        d.update(updates)

        html = u'''
<form id="searchform" method="get" action="%(url)s">
<div>
<input type="hidden" name="action" value="fullsearch">
<input type="hidden" name="context" value="180">
<label for="searchinput">%(search_label)s</label>
<input id="searchinput" type="text" name="value" value="%(search_value)s" placeholder="Search">
<input id="titlesearch" name="titlesearch" type="submit" value="%(search_title_label)s">
<input id="fullsearch" name="fullsearch" type="submit" value="%(search_full_label)s">
</div>
</form>
''' % d
        return html

    def html_head(self, d):
        """ Assemble html head

        @param d: parameter dictionary
        @rtype: unicode
        @return: html head
        """
        html = [
            u'<meta name="viewport" content="width=device-width">',
            u'<link rel="icon" type="image/x-icon" href="%(static)s/fsfe/img/favicon.ico">' % {
                'static': self.cfg.url_prefix_static,
            },
            u'<title>%(title)s - %(sitename)s</title>' % {
                'title': wikiutil.escape(d['title']),
                'sitename': wikiutil.escape(d['sitename']),
            },
            self.html_stylesheets(d),
            self.rsslink(d),
            self.universal_edit_button(d),
            ]
        return '\n'.join(html)

    def header(self, d, **kw):
        """ Assemble wiki header

        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # Header
            u'<div id="header">',
            self.username(d),
            self.msg(d),
            self.logo(), self.navibar(d), self.searchform(d),
            self.trail(d),
            u'</div>',

            # Start of page
            self.startPage(),
            u'<h1 id="locationline">', self.interwiki(d), self.title_with_separators(d), u'</h1>',
            self.editbar(d),
            u'<input type="checkbox" id="toggle_comments" name="comments"><label for="toggle_comments">Comments</label>',
        ]
        return u'\n'.join(html)

    editorheader = header

    def footer(self, d, **keywords):
        """ Assemble wiki footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """
        page = d['page']
        html = [
            # End of page
            self.pageinfo(page),
            self.editbar(d),
            self.endPage(),

            # Footer
            u'<div id="footer">',
            self.credits(d),
            self.showversion(d, **keywords),
            u'</div>',
            ]
        return u'\n'.join(html)

def execute(request):
    """
    Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)


